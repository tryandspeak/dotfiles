#!/bin/bash

i3addons=(
	"https://github.com/ganifladi/blurme"
)

setup() {
	echo "Set up ~/.i3 dir"
	mkdir ~/.i3
	echo "Sym linking i3 config to git repo"
	ln -s `pwd`/i3config ~/.i3/config
	echo "Sym linking Xresources to git repo"
	ln -s `pwd`/Xresources ~/.Xresources
}

clean() {
	echo "remove old .i3 folder"
	rm -rf ~/.i3
}

check_git() {
	git --version
	if [ $? -eq 127 ]
	then
		echo "Git is not installed or is not on your path. Install git and try again"
		exit 0;
	fi
}

echo "WARNING: This install script will delete your .i3 directory"
echo "Please back it up if you would like to save any of your settings"
read -p "Continue with install [Yy]? " -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
	check_git
	clean
	setup
	cd ~/.i3
	for i in ${i3addons[@]}; do
		echo "--> Downloading and installing plugin ${i}"
		git clone ${i}
	done

	echo "********************************************************"
	echo
else
	echo "Aborted install!!!"
fi

exit 0
