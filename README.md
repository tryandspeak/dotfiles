Vim - setup and install
=======================
This is my personal .vimrc as well as an install script to get
you up and going on a new system. I am using pathogen to
manage all the plugins. The install script with fetch each plugin
and install it into ~/.vim/bundles which pathogen with then add
to your vim runtime files.

Fonts
-----
I use Hack on Linux. It can probably be found with your package manager. If using
apt-get you can install with:

`sudo apt-get install fonts-hack-ttf`

Plugins
=======
Below is a brief description of each plugin and how they are configured in my vimrc.
Unless otherwise specified the default key bindings for each plugin are used.
Check the link to the plugin repository for more information on plugin features and
any key bindings that the plugin may use.

supertab
--------
Allows you to use the [Tab] key for symbol completion. This is bound to omnicomplete, by pressing
[Tab] [Tab] while typing a symbol omnicomplete will be called to complete the symbol.
* https://github.com/ervandew/supertab.git

tcomment
--------
TComment works like a toggle, i.e., it will comment out text that contains
uncommented lines, and it will remove comment markup for already commented text.
* https://github.com/tomtom/tcomment_vim

tagbar
------
The tabbar plugin allows you to view all of the symbols in a given file.
This plugin has been mapped to F4
* https://github.com/majutsushi/tagbar

DoxygenToolkit
--------------
Automatically insert doxygen comments into a file.
* https://github.com/vim-scripts/DoxygenToolkit.vim

vim-color-solarized
-------------------
A nice looking color scheme.
* https://github.com/altercation/vim-colors-solarized.git"

ctrlp
-----
Allows quick access to all the files in a given project.
This plugin has been mapped to Control-P
* https://github.com/kien/ctrlp.vim

vim-airline
---------
Gives a nice status line and integrates with tagbar to
show what function you are currently in.
* https://github.com/bling/vim-airline

ale
---------
-- Has replaced syntastic because linters are executed asynchronously --
Highlights errors in your code. Hooks into a languages linter
so any errors will be highlighted.
* https://github.com/w0rp/ale.git

vim-fugitive
------------
Integrates with vim-airline to give info on the git current working
branch and other lots of cool features
* https://github.com/tpope/vim-fugitive

NERDTree
--------
Nice file browser integration. NERDTreeToggle has been mapped to F5
* https://github.com/scrooloose/nerdtree

i3 - setup and install
=======================
My config for the i3 window manager, assumes i3-gaps.
Uses vim keybindings instead of the default off by one vim bindings.
Horizontal split is mapped to `Mod1+z`
